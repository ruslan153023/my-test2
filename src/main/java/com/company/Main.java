package com.company;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.List;

public class Main {

    public static String[] a;
    public static int i;
    public static String b;

    public static void main(String[] args) {

        System.setProperty("webdriver.chrome.driver", "C:\\Users\\Ruslan\\Downloads\\chromedriver_win32\\chromedriver.exe");
        WebDriver driver= new ChromeDriver();
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));

        try {
            driver.get("http://old.refactorx.ru/SitePages/Спектр%20услуг.aspx");
            Thread.sleep(1);

            // WebElement vhod = (new WebDriverWait(driver, Duration.ofSeconds(30)))
            //     .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='Вход']")));
            //     vhod.click();

            // WebElement spravka = (new WebDriverWait(driver, Duration.ofSeconds(30))) //     .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[@title='Справка']")));
            //     spravka.click();

            WebElement refact = (new WebDriverWait(driver, Duration.ofSeconds(30)))
                    .until(ExpectedConditions.presenceOfElementLocated(By.id("siteIcon")));
            refact.click();

          //  driver.findElement(By.xpath("//span[text()='Контакты']")).click();

          //  WebElement contact = driver.findElement(By.xpath("//a[text()='Как с нами связаться']"));
          //contact.click();

          //driver.findElement(By.xpath("//tr/td[text()='Ваше Имя']/../td[2]/input")).sendKeys("Руслан");
          //driver.findElement(By.xpath("//tr/td[text()='Компания']/../td[2]/input")).sendKeys("Гугл");
          //driver.findElement(By.xpath("//tr/td[text()='E-mail']/../td[2]/input")).sendKeys("mail@mail.ru");
          //driver.findElement(By.xpath("//tr/td[text()='Телефон']/../td[2]/input")).sendKeys("88009997785");
          //driver.findElement(By.xpath("//tr/td[text()='Ваши вопросы']/../td[2]/textarea")).sendKeys("Как стать автоматизатором?");
          //driver.findElement(By.xpath("//div[text()='Введите текст на картинке']/input")).sendKeys("4282");
          //driver.findElement(By.xpath("//input[@value='Отправить']")).click();
          //driver.findElement(By.xpath("//a[text()='Как с нами связаться']")).click();

            a = new String[]{"none", "Проектирование и разработка", "Продукты", "Наша команда", "Как нас найти"};

            for (i = 1; i < a.length; i++) {
                List <WebElement> vkladka = driver.findElements(By.xpath("//ul[@class = 'static']/li/a/span/span[@class='menu-item-text']"));
                b = "//a[text()='" + a[i] + "']";
                vkladka.get(i).click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(b)));
                driver.findElement(By.xpath(b)).click();
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            driver.quit();
        }
    }
}
